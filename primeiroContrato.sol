pragma solidity ^0.4.24;

contract primeiroContrato {
    
    string PNome;
    uint idade;
    
    event Personagem(string nome,uint idade);
    
    function setPersonagem(string _PNome, uint _idade) public {
        PNome = _PNome;
        idade = _idade;
        emit Personagem(_PNome, _idade);
    }
    
    function getPersonagem() public constant returns(string, uint){
        return (PNome, idade);
    }
    
}